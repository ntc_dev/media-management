<?php

namespace NTCDev\Media\App\Providers;

use Illuminate\Support\ServiceProvider;

class MediaServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
//        $src_path = realpath(__DIR__ . '/../../');
//        $config_path = realpath($src_path . '/configs');
//        $this->mergeConfigFrom(
//            realpath($config_path . '/ntcdev_media.php'), 'ntcdev_media'
//        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $src_path = realpath(__DIR__ . '/../../');
        $route_path = realpath($src_path . '/routes');
        $view_path = realpath($src_path . '/views');
        $translation_path = realpath($src_path . '/translations');
        $migration_path = realpath($src_path . '/migrations');
        $config_path = realpath($src_path . '/configs');

        $this->loadViewsFrom($view_path, 'ntcdev_media');
        $this->loadTranslationsFrom($translation_path, 'ntcdev_media');
        $this->loadMigrationsFrom($migration_path);
        $this->loadRoutesFrom(realpath($route_path . '/admin.php'));
        $this->publishes([
            realpath($config_path . '/ntcdev_media.php') => config_path('ntcdev_media.php')
        ], 'ntcdev_media_config');
    }
}
